# Commons

[![Maven Central status](https://img.shields.io/maven-central/v/io.ultreia.java4all/jaxx.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.ultreia.java4all%22%20AND%20a%3A%22commons%22)
![Build Status](https://gitlab.com/java4all/commons/badges/develop/build.svg)

# Resources

* [Changelog](https://gitlab.com/java4all/commons/blob/develop/CHANGELOG.md)
* [Documentation](http://java4all.gitlab.io/commons)

# Community

TODO: create mailing list for the project.

# History

This project is a fork of three other projects, keeping only good parts :

* [nuiton-utils (v3.0-rc-18)](https://gitlab.nuiton.org/nuiton/nuiton-utils)
* [nuiton-converter (v1.0)](https://gitlab.nuiton.org/nuiton/nuiton-converter)
* [nuiton-version (v1.0-rc-2)](https://gitlab.nuiton.org/nuiton/nuiton-version)

All packages names were kept to make it simpler for migrations.
