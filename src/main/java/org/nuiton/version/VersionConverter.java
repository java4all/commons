package org.nuiton.version;

/*
 * #%L
 * Commons
 * %%
 * Copyright (C) 2017 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.converter.NuitonConverter;

/**
 * To convert {@link Version} using {@link Converter} API.
 *
 * Created on 7/11/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see Version
 * @since 1.0
 */
public class VersionConverter implements NuitonConverter<Version> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VersionConverter.class);

    public VersionConverter() {
        if (log.isDebugEnabled()) {
            log.debug("init version converter " + this);
        }
    }

    @Override
    public Class<Version> getType() {
        return Version.class;
    }

    @Override
    public <T> T convert(Class<T> aClass, Object value) {
        if (value == null) {
            throw new ConversionException(
                    String.format("No value specified for converter %s", this));
        }
        if (isEnabled(aClass)) {
            Object result;
            if (isEnabled(value.getClass())) {
                result = value;
                return aClass.cast(result);
            }
            if (value instanceof String) {
                try {
                    result = VersionBuilder.create((String) value).build();
                    return aClass.cast(result);
                } catch (IllegalArgumentException e) {
                    throw new ConversionException(
                            String.format("Could not convert version %1$s with converter %2$s for reason \\: %3$s", value, this, e.getMessage()), e);
                }
            }
        }
        throw new ConversionException(
                String.format("no convertor found for type %2$s and objet '%1$s'", aClass.getName(), value));
    }

    protected boolean isEnabled(Class<?> aClass) {
        return Version.class.equals(aClass);
    }
}
