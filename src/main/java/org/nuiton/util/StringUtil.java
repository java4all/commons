/*
 * #%L
 * Commons
 * %%
 * Copyright (C) 2017 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import org.apache.commons.lang3.SystemUtils;

import java.text.MessageFormat;
import java.util.Locale;

/**
 * Classe contenant un ensemle de methode static utiles pour la manipulation des
 * chaine de caractere mais qui ne sont pas defini dans la classe String de
 * Java.
 * <p>
 * Created: 21 octobre 2003
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * @author Tony Chemit - dev@tchemit.fr
 */
public class StringUtil { // StringUtil

    public static final String[] EMPTY_STRING_ARRAY = new String[0];

    /** Constructor for the StringUtil object */
    protected StringUtil() {
    }

    /**
     * Know if a string is a valid e-mail.
     *
     * @param str a string
     * @return true if {@code str} is syntactically a valid e-mail address
     * @since 2.1
     */
    public static boolean isEmail(String str) {
        return str.matches("^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+((\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)?)+@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\\-]*[a-zA-Z0-9])?$");
    }

    private static final double[] timeFactors = {1000000, 1000, 60, 60, 24};

    private static final String[] timeUnites = {"ns", "ms", "s", "m", "h", "d"};

    /**
     * Converts an time delay into a human readable format.
     *
     * @param value the delay to convert
     * @return the memory representation of the given value
     * @see #convert(long, double[], String[])
     */
    public static String convertTime(long value) {
        return convert(value, timeFactors, timeUnites);
    }

    /**
     * Converts an time period into a human readable format.
     *
     * @param value  the begin time
     * @param value2 the end time
     * @return the time representation of the given value
     * @see #convert(long, double[], String[])
     */
    public static String convertTime(long value, long value2) {
        return convertTime(value2 - value);
    }

    static final protected double[] memoryFactors = {1024, 1024, 1024, 1024};

    static final protected String[] memoryUnites = {"o", "Ko", "Mo", "Go", "To"};

    /**
     * Converts an memory measure into a human readable format.
     *
     * @param value the memory measure to convert
     * @return the memory representation of the given value
     * @see #convert(long, double[], String[])
     */
    public static String convertMemory(long value) {
        return convert(value, memoryFactors, memoryUnites);
    }

    /**
     * Note: this method use the current locale
     * (the {@link Locale#getDefault()}) in the method
     * {@link MessageFormat#MessageFormat(String)}.
     *
     * @param value   value to convert
     * @param factors facotrs used form conversion
     * @param unites  libelle of unites to use
     * @return the converted representation of the given value
     */
    public static String convert(long value, double[] factors, String[] unites) {
        long sign = value == 0 ? 1 : value / Math.abs(value);
        int i = 0;
        double tmp = Math.abs(value);
        while (i < factors.length && i < unites.length && tmp > factors[i]) {
            tmp = tmp / factors[i++];
        }

        tmp *= sign;
        String result;
        result = MessageFormat.format("{0,number,0.###}{1}", tmp,
                unites[i]);
        return result;
    }

    /**
     * Convertir un nom en une constante Java
     * <p>
     * Les seuls caractères autorisés sont les alpha numériques, ains
     * que l'underscore. tous les autres caractères seront ignorés.
     *
     * @param name le nom à convertir
     * @return la constante générée
     */
    public static String convertToConstantName(String name) {
        StringBuilder sb = new StringBuilder();
        char lastChar = 0;
        for (int i = 0, j = name.length(); i < j; i++) {
            char c = name.charAt(i);
            if (Character.isDigit(c)) {
                sb.append(c);
                lastChar = c;
                continue;
            }
            if (!Character.isLetter(c)) {
                if (lastChar != '_') {
                    sb.append('_');
                }
                lastChar = '_';
                continue;
            }
            if (Character.isUpperCase(c)) {
                if (!Character.isUpperCase(lastChar) && lastChar != '_') {
                    sb.append('_');
                }
                sb.append(c);
            } else {
                sb.append(Character.toUpperCase(c));
            }
            lastChar = c;
        }
        String result = sb.toString();
        // clean tail
        while (!result.isEmpty() && result.endsWith("_")) {
            result = result.substring(0, result.length() - 1);
        }
        // clean head
        while (!result.isEmpty() && result.startsWith("_")) {
            result = result.substring(1);
        }
        return result;
    }

    /**
     * Contract to use in {@link StringUtil#join(Iterable, ToString, String, boolean) }
     * method. This will provide a toString method to convert an object in a
     * string.
     *
     * @param <O> type of object manipulated
     */
    public interface ToString<O> {

        /**
         * Convert an object o in a string.
         *
         * @param o to convert
         * @return the string for this object o
         */
        String toString(O o);
    }

    /**
     * Used to concat an {@code iterable} of Object separated
     * by {@code separator} using the toString() method of each object.
     * You can specify if the string must be trimmed or not.
     *
     * @param iterable  Iterable with objects to treate
     * @param separator to used
     * @param trim      if each string must be trim
     * @return the String chain of all elements separated by separator, never
     *         return null, will return an empty String for an empty list.
     */
    public static String join(Iterable<?> iterable, String separator,
                              boolean trim) {
        return join(iterable, null, separator, trim);
    }

    /**
     * Used to concat an {@code iterable} of object {@code <O>} separated by
     * {@code separator}. This method need a {@code ts} contract to
     * call on each object. The ToString can be null to use directly the
     * toString() method on the object. The {@code trim} boolean is used
     * to specify if each string object has to be trimmed. The null elements
     * in the {@code list} will be ignored.
     *
     * @param <O>       type of object in the list
     * @param iterable  Iterable with objects to treate
     * @param ts        used to specify how the object is converted in String
     * @param separator to used between each object string
     * @param trim      if trim() method need to by apply on each object string
     * @return the String chain of all elements separated by separator, never
     *         return null, will return an empty String for an empty list.
     * @throws NullPointerException if iterable is {@code null}.
     */
    public static <O> String join(Iterable<O> iterable, ToString<O> ts,
                                  String separator, boolean trim) throws NullPointerException {
        if (iterable == null) {
            throw new NullPointerException("null iterable can't be used" +
                    " to join the elements with " + separator);
        }
        // Do nothing for an empty list
        if (!iterable.iterator().hasNext()) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for (O o : iterable) {
            // Ignore the null object in the list
            if (o == null) {
                continue;
            }
            String str;
            // Use ToString contract from argument
            if (ts != null) {
                str = ts.toString(o);
                // Or call toString() method directly on object
            } else {
                str = o.toString();
            }
            // Apply trim if needed
            if (trim) {
                str = str.trim();
            }
            builder.append(separator).append(str);
        }
        // Suppress the first separator at beginning of the chain
        String result = builder.substring(separator.length());
        return result;
    }

    /**
     *
     * @return the file separator escaped for a regex regarding the os used.
     */
    public static String getFileSeparatorRegex() {
        String result;
        if(SystemUtils.IS_OS_WINDOWS) {
            result = "\\\\";
        } else {
            result = "/";
        }
        return result;
    }
}
