package org.nuiton.converter;

/*
 * #%L
 * Commons
 * %%
 * Copyright (C) 2017 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;

/**
 * To convert {@link KeyStroke} from and to {@link String}.
 *
 * @author Sylvain Letellier - letellier@codelutin.com
 * @since 1.0
 */
public class KeyStrokeConverter implements NuitonConverter<KeyStroke> {

    private static final Log LOGGER = LogFactory.getLog(KeyStrokeConverter.class);

    @Override
    public <T> T convert(Class<T> aClass, Object value) {
        if (value == null) {
            throw new ConversionException(
                    String.format("No value specified for converter %s", this));
        }
        if (isEnabled(aClass)) {
            Object result;
            if (isEnabled(value.getClass())) {
                result = value;
                return aClass.cast(result);
            }
            if (value instanceof String) {
                result = KeyStroke.getKeyStroke((String) value);
                return aClass.cast(result);
            }
        }
        throw new ConversionException(
                String.format("no convertor found for type %2$s and objet '%1$s'", aClass.getName(), value));
    }

    protected boolean isEnabled(Class<?> aClass) {
        return KeyStroke.class.equals(aClass);
    }

    @Override
    public Class<KeyStroke> getType() {
        return KeyStroke.class;
    }

    public KeyStrokeConverter() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("init keystroke converter " + this);
        }
    }

}
