package org.nuiton.converter;

/*
 * #%L
 * Commons
 * %%
 * Copyright (C) 2017 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.beanutils.Converter;

/**
 * Over {@link Converter} contract to be able to get main type of a converter.
 * <p/>
 * Created on 7/23/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public interface NuitonConverter<O> extends Converter {

    /**
     * @return the main type of the converter.
     */
    Class<O> getType();
}
