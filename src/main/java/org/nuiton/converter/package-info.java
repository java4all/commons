/**
 * This package contains the converter api + the format api.
 * <p/>
 * <h1>Converter api</h1>
 * This converter api is based on the
 * http://commons.apache.org/beanutils {@code commons-beanutils}.
 * <p>
 * With a thin over layer (see {@link org.nuiton.converter.NuitonConverter} to get at loading
 * the type of converter to register.
 * <p/>
 * Use the {@link org.nuiton.converter.ConverterUtil} to register or obtain a
 * converter.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
package org.nuiton.converter;

/*
 * #%L
 * Commons
 * %%
 * Copyright (C) 2017 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
