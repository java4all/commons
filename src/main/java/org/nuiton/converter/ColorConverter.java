package org.nuiton.converter;

/*
 * #%L
 * Commons
 * %%
 * Copyright (C) 2017 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.*;
import java.util.Scanner;

/**
 * To convert {@link Color} from and to {@link String}.
 *
 * @author mallon - mallon@codelutin.com
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ColorConverter implements NuitonConverter<Color> {

    /** Logger. */
    private static final Log LOGGER = LogFactory.getLog(ColorConverter.class);

    @Override
    public <T> T convert(Class<T> aClass, Object value) {
        if (!isEnabled(aClass)) {
            throw new ConversionException("unsupported type: " + aClass);
        }

        String valueToString = (String) value;
        /*
         * To get color from a formatted string
         * Two formatting cases :
         * - 'java.awt.Color[r=255,g=51,b=51]', for example
         * - hexa, like '#000000'
         */
        Color result;
        try {
            if (valueToString.length() == 7 && valueToString.charAt(0) == '#') {
                result = new Color(Integer.parseInt(valueToString.substring(1), 16));
            } else {
                Scanner sc = new Scanner(valueToString);
                sc.useDelimiter("\\D+");
                result = new Color(sc.nextInt(), sc.nextInt(), sc.nextInt());
            }
            return aClass.cast(result);
        } catch (Exception e) {
            throw new ConversionException(
                    "colors must be of the form #xxxxxx ('#' followed by " +
                            "six hexadecimal digits), or the name of a constant " +
                            "field in java.awt.Color (found: '" + valueToString + "')",
                    e);
        }
    }

    @Override
    public Class<Color> getType() {
        return Color.class;
    }

    public ColorConverter() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("init color converter " + this);
        }
    }

    protected boolean isEnabled(Class<?> aClass) {
        return Color.class.equals(aClass);
    }
}
