package org.nuiton.version;

/*
 * #%L
 * Commons
 * %%
 * Copyright (C) 2017 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created on 7/10/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class VersionTest {

    @Test
    public void testEquals() {

        // Ignore case on string componants
        assertEquals(Versions.valueOf("1.1-rc"), Versions.valueOf("1.1-Rc"));

        // Ignore componants join separator
        assertEquals(Versions.valueOf("1.1"), VersionBuilder.create(Lists.<Comparable>newArrayList(1, 1)).setJoinSeparator(' ').build());

    }

    protected void buildVersion(Version version,
                                String stringRepresentation,
                                int nbComponants,
                                boolean snapshot,
                                Comparable... componants) {

        Assert.assertEquals("Bad string represetion", stringRepresentation, version.getVersion());
        Assert.assertEquals("Bad number of componants", nbComponants, version.getComponantCount());
        Assert.assertEquals("Bad snapshot value", snapshot, version.isSnapshot());
        for (int i = 0; i < componants.length; i++) {
            Comparable componant = componants[i];
            Assert.assertEquals("Bad componant value at index " + i, componant, version.getComponant(i).getValue());
        }

    }

    protected void assertEquals(Version v1, Version v2) {
        Assert.assertTrue(v1 + " equals to " + v2, v1.equals(v2));
    }

}
