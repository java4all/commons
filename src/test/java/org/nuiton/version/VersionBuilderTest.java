package org.nuiton.version;

/*
 * #%L
 * Commons
 * %%
 * Copyright (C) 2017 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created on 7/10/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class VersionBuilderTest {

    @Test
    public void testBuildVersionFromString() {

        buildVersion(VersionBuilder.create().build(), "0", 1, false, 0);
        buildVersion(VersionBuilder.create().setSnapshot(true).build(), "0-SNAPSHOT", 1, true, 0);
        buildVersion(VersionBuilder.create("0").build(), "0", 1, false, 0);
        buildVersion(VersionBuilder.create("1").build(), "1", 1, false, 1);
        buildVersion(VersionBuilder.create("12.3").build(), "12.3", 2, false, 12, 3);
        buildVersion(VersionBuilder.create("12.34-alpha-56").build(), "12.34-alpha-56", 4, false, 12, 34, "alpha", 56);
        buildVersion(VersionBuilder.create("1.1-beta-1").build(), "1.1-beta-1", 4, false, 1, 1, "beta", 1);
        buildVersion(VersionBuilder.create("1.1-rc-1").build(), "1.1-rc-1", 4, false, 1, 1, "rc", 1);
        buildVersion(VersionBuilder.create("1.1-rc-1-SNAPSHOT").build(), "1.1-rc-1-SNAPSHOT", 4, true, 1, 1, "rc", 1);
        buildVersion(VersionBuilder.create("1.1-rc2").build(), "1.1-rc2", 4, false, 1, 1, "rc", 2);
        buildVersion(VersionBuilder.create("1.1-rc2a").build(), "1.1-rc2a", 5, false, 1, 1, "rc", 2, "a");

    }

    @Test
    public void testBuildVersionFromComponants() {

        buildVersion(VersionBuilder.create(Lists.<Comparable>newArrayList(0)).build(), "0", 1, false, 0);
        buildVersion(VersionBuilder.create(Lists.<Comparable>newArrayList(1)).build(), "1", 1, false, 1);
        buildVersion(VersionBuilder.create(Lists.<Comparable>newArrayList(12, 3)).build(), "12.3", 2, false, 12, 3);
        buildVersion(VersionBuilder.create(Lists.<Comparable>newArrayList(12, 34, "alpha", 56)).build(), "12.34.alpha.56", 4, false, 12, 34, "alpha", 56);
        buildVersion(VersionBuilder.create(Lists.<Comparable>newArrayList(1, 1, "beta", 1)).build(), "1.1.beta.1", 4, false, 1, 1, "beta", 1);
        buildVersion(VersionBuilder.create(Lists.<Comparable>newArrayList(1, 1, "rc", 1)).build(), "1.1.rc.1", 4, false, 1, 1, "rc", 1);
        buildVersion(VersionBuilder.create(Lists.<Comparable>newArrayList(1, 1, "rc", 1)).setSnapshot(true).build(), "1.1.rc.1-SNAPSHOT", 4, true, 1, 1, "rc", 1);
        buildVersion(VersionBuilder.create(Lists.<Comparable>newArrayList(1, 1, "rc", 2)).build(), "1.1.rc.2", 4, false, 1, 1, "rc", 2);
        buildVersion(VersionBuilder.create(Lists.<Comparable>newArrayList(1, 1, "rc", 2, "a")).build(), "1.1.rc.2.a", 5, false, 1, 1, "rc", 2, "a");

    }


    protected void buildVersion(Version version,
                                String stringRepresentation,
                                int nbComponants,
                                boolean snapshot,
                                Comparable... componants) {

        Assert.assertEquals("Bad string represetion", stringRepresentation, version.getVersion());
        Assert.assertEquals("Bad number of componants", nbComponants, version.getComponantCount());
        Assert.assertEquals("Bad snapshot value", snapshot, version.isSnapshot());
        for (int i = 0; i < componants.length; i++) {
            Comparable componant = componants[i];
            Assert.assertEquals("Bad componant value at index " + i, componant, version.getComponant(i).getValue());
        }

    }

}
