package org.nuiton.version;

import org.junit.Assert;
import org.junit.Test;

/*
 * #%L
 * Commons
 * %%
 * Copyright (C) 2017 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Test about {@link Versions} utility class.
 * 
 * @author Eric Chatellier
 */
public class VersionsTest {

    /**
     * Test to create a new version by extracting a single component.
     */
    @Test
    public void testExtractSingleComponent() {
        Version initVersion = Versions.valueOf("10.20.30.40");
        Version subVersion = Versions.extractVersion(initVersion, 2);
        Assert.assertEquals(Versions.valueOf("30"), subVersion);
    }
    
    /**
     * Test to create a new version by extracting a components sub set.
     */
    @Test
    public void testExtractTwoComponents() {
        Version initVersion = Versions.valueOf("10.20.30.40");
        Version subVersion = Versions.extractVersion(initVersion, 2, 3);
        Assert.assertEquals(Versions.valueOf("30.40"), subVersion);
    }
    
    /**
     * Test to create a new version by extracting with illegal parameters.
     */
    @Test(expected=IllegalArgumentException.class)
    public void testExtractIllegalParams() {
        Version initVersion = Versions.valueOf("10.20.30.40");
        Version subVersion = Versions.extractVersion(initVersion, 3, 2);
    }

    @Test
    public void testIncrements() {

        Version[] versions = new Version[]{
                VersionBuilder.create().setSnapshot(true).build(),
                VersionBuilder.create().build(),
                Versions.valueOf("1"),
                Versions.valueOf("1.1-alpha-1"),
                Versions.valueOf("1.1-alpha-2"),
                Versions.valueOf("1.1-beta-1"),
                Versions.valueOf("1.1-rc-1-SNAPSHOT"),
                Versions.valueOf("1.1-rc-1"),
                Versions.valueOf("1.1-rc2"),
                Versions.valueOf("1.1"),
                Versions.valueOf("1.1.1"),
                Versions.valueOf("1.1.1-blablah"),
                Versions.valueOf("1.1.1-blablah1-SNAPSHOT"),
                Versions.valueOf("1.1.1-blablah1"),
                Versions.valueOf("1.1.1-blablah1a"),
                Versions.valueOf("1.1.1-blablah1b")
        };

        Version[] versionsIncrements = new Version[]{
                Versions.valueOf("1-SNAPSHOT"),
                Versions.valueOf("1"),
                Versions.valueOf("2"),
                Versions.valueOf("1.1-alpha-2"),
                Versions.valueOf("1.1-alpha-3"),
                Versions.valueOf("1.1-beta-2"),
                Versions.valueOf("1.1-rc-2-SNAPSHOT"),
                Versions.valueOf("1.1-rc-2"),
                Versions.valueOf("1.1-rc3"),
                Versions.valueOf("1.2"),
                Versions.valueOf("1.1.2"),
                Versions.valueOf("1.1.1-blablah.1"),
                Versions.valueOf("1.1.1-blablah2-SNAPSHOT"),
                Versions.valueOf("1.1.1-blablah2"),
                Versions.valueOf("1.1.1-blablah1a.1"),
                Versions.valueOf("1.1.1-blablah1b.1")
        };

        for (int i = 0, l = versions.length; i < l; i++) {

            Version v = versions[i];
            Version vIncrements = versionsIncrements[i];
            Version increments = Versions.increments(v);
            Assert.assertTrue(v + " + 1 = " + increments + " should be " + vIncrements, increments.equals(vIncrements));
            Assert.assertTrue(v + " + 1 = " + increments + " should be " + vIncrements, increments.getVersion().equals(vIncrements.getVersion()));

        }

    }
}
