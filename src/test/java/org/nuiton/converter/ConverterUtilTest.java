package org.nuiton.converter;

/*
 * #%L
 * Commons
 * %%
 * Copyright (C) 2017 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.junit.Assert;
import org.junit.Test;

import java.net.URI;

/**
 * ConverterUtil Tester.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @version 1.0
 * @since <pre>02/13/2008</pre>
 */
public class ConverterUtilTest {

    @Test
    public void testInitConverters() {

        ConverterUtil.deregister();

        Converter t = ConvertUtils.lookup(URI.class);
        Assert.assertNull(t);
        ConverterUtil.initConverters();
        t = ConvertUtils.lookup(URI.class);
        Assert.assertNotNull(t);
    }

    @Test
    public void testConvert() throws Exception {
        String s;
        s = "";
        Assert.assertEquals(
                new String(s.getBytes()),
                new String(ConverterUtil.convert(s.toCharArray())));

        s = "a";
        Assert.assertEquals(
                new String(s.getBytes()),
                new String(ConverterUtil.convert(s.toCharArray())));

        s = "kZZFIOFEIOFEfdskdfmldsfjklsfjldfldfjdsfiosabcd4567'''`~teAZEst";
        Assert.assertEquals(
                new String(s.getBytes()),
                new String(ConverterUtil.convert(s.toCharArray())));

        s = "]{}{}{{[#{[{#[#]{][{^#][^]#{^[]{#][#]{]@[{#][^#{][^]#{teAZEst";
        Assert.assertEquals(
                new String(s.getBytes()),
                new String(ConverterUtil.convert(s.toCharArray())));

        // FIXME following test won't pass
        //s = "éééééé]{}{}{{[#{[{#[#]{][{^#][^]#{^[]{#][#]{]@[{#][^#{][^]#{teAZEst";
        //assertEquals(new String(s.getBytes()), new String(ConverterUtil.convert(s.toCharArray())));

    }
}
