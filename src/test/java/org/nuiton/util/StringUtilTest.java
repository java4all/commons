/*
 * #%L
 * Commons
 * %%
 * Copyright (C) 2017 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.assertEquals;

public class StringUtilTest {

    @Test
    public void testConvert() throws Exception {
        assertEquals("365d", StringUtil.convertTime(31536000000000000L));
        assertEquals("2d", StringUtil.convertTime(172800000000000L));
        assertEquals("2h", StringUtil.convertTime(7200000000000L));
        assertEquals("2m", StringUtil.convertTime(120000000000L));

        assertEquals("2s", StringUtil.convertTime(2000000002L));
        assertEquals("2s", StringUtil.convertTime(2000000000L));
        assertEquals("2ms", StringUtil.convertTime(2000000L));
        assertEquals("2ns", StringUtil.convertTime(2L));
        assertEquals("0ns", StringUtil.convertTime(0L));

        assertEquals("0o", StringUtil.convertMemory(0L));
        assertEquals("2o", StringUtil.convertMemory(2L));
        assertEquals("2Ko", StringUtil.convertMemory(2048L));
        assertEquals("2Mo", StringUtil.convertMemory(2097152L));
        assertEquals("2Mo", StringUtil.convertMemory(2097154L));

        assertEquals("2Go", StringUtil.convertMemory(2147483648L));
        assertEquals("2To", StringUtil.convertMemory(2199023255552L));
        assertEquals("2000To", StringUtil.convertMemory(2199023255552000L));

        assertEquals("-2Mo", StringUtil.convertMemory(-2097152L));
        assertEquals("-2Mo", StringUtil.convertMemory(-2097154L));

        Locale oldLocale = Locale.getDefault();
        // test in french locale
        Locale.setDefault(Locale.FRENCH);
        assertEquals("2,02s", StringUtil.convertTime(2020000002L));
        assertEquals("2,094Mo", StringUtil.convertMemory(2196152L));
        assertEquals("-2,094Mo", StringUtil.convertMemory(-2196152L));

        // test in english locale
        Locale.setDefault(Locale.ENGLISH);
        assertEquals("2.02s", StringUtil.convertTime(2020000002L));
        assertEquals("2.094Mo", StringUtil.convertMemory(2196152L));
        assertEquals("-2.094Mo", StringUtil.convertMemory(-2196152L));

        // push back previous locale
        Locale.setDefault(oldLocale);
    }

    @Test
    public void testConvertToConstantName() throws Exception {
        assertEquals("YES", StringUtil.convertToConstantName("yes"));
        assertEquals("YES", StringUtil.convertToConstantName("*$$?YEs"));
        assertEquals("YES", StringUtil.convertToConstantName("_yes!$*_"));
        assertEquals("YES", StringUtil.convertToConstantName("_Yes____"));

        assertEquals("YES_OR_NO", StringUtil.convertToConstantName("__yesOrNo_"));
        assertEquals("YES_OR_NO", StringUtil.convertToConstantName("Yes-or-!*=No"));
        assertEquals("YES_OR_NO", StringUtil.convertToConstantName("_yes__or__no"));
        assertEquals("YES_OR_NO", StringUtil.convertToConstantName("_YesOR___No"));

    }

}

