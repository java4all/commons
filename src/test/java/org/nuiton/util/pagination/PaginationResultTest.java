package org.nuiton.util.pagination;

/*
 * #%L
 * Commons
 * %%
 * Copyright (C) 2017 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class PaginationResultTest {

    protected PaginationResult<Object> paginationResult;

    @Before
    public void init() {
        List<Object> elements = Lists.newArrayList();
        PaginationParameter page = PaginationParameter.of(2, 50);
        paginationResult = PaginationResult.of(elements, 204, page);
    }

    @Test
    public void testGetFirstPage() {
        Assert.assertEquals(0, paginationResult.getFirstPage().getPageNumber());
        Assert.assertEquals(50, paginationResult.getFirstPage().getPageSize());
        Assert.assertEquals(0, paginationResult.getFirstPage().getStartIndex());
        Assert.assertEquals(49, paginationResult.getFirstPage().getEndIndex());

        List<Object> elements = Lists.newArrayList();
        PaginationResult<Object> firstPageResult = PaginationResult.of(elements, paginationResult.getCount(), paginationResult.getFirstPage());
        Assert.assertTrue(firstPageResult.hasNextPage());
    }

    @Test
    public void testGetPreviousPage() {
        Assert.assertEquals(1, paginationResult.getPreviousPage().getPageNumber());
        Assert.assertEquals(50, paginationResult.getPreviousPage().getPageSize());
        Assert.assertEquals(50, paginationResult.getPreviousPage().getStartIndex());
        Assert.assertEquals(99, paginationResult.getPreviousPage().getEndIndex());

        List<Object> elements = Lists.newArrayList();
        PaginationResult<Object> previousPageResult = PaginationResult.of(elements, paginationResult.getCount(), paginationResult.getPreviousPage());
        Assert.assertTrue(previousPageResult.hasNextPage());
    }

    @Test
    public void testCurrentPage() {
        Assert.assertEquals(5, paginationResult.getPageCount());
        Assert.assertEquals(2, paginationResult.getCurrentPage().getPageNumber());
        Assert.assertEquals(50, paginationResult.getCurrentPage().getPageSize());
        Assert.assertEquals(100, paginationResult.getCurrentPage().getStartIndex());
        Assert.assertEquals(149, paginationResult.getCurrentPage().getEndIndex());

        Assert.assertTrue(paginationResult.hasNextPage());
    }

    @Test
    public void testGeNextPage() {
        Assert.assertEquals(3, paginationResult.getNextPage().getPageNumber());
        Assert.assertEquals(50, paginationResult.getNextPage().getPageSize());
        Assert.assertEquals(150, paginationResult.getNextPage().getStartIndex());
        Assert.assertEquals(199, paginationResult.getNextPage().getEndIndex());

        List<Object> elements = Lists.newArrayList();
        PaginationResult<Object> nextPageResult = PaginationResult.of(elements, paginationResult.getCount(), paginationResult.getNextPage());
        Assert.assertTrue(nextPageResult.hasNextPage());
    }

    @Test
    public void testGetLastPage() {
        Assert.assertEquals(4, paginationResult.getLastPage().getPageNumber());
        Assert.assertEquals(50, paginationResult.getLastPage().getPageSize());
        Assert.assertEquals(200, paginationResult.getLastPage().getStartIndex());
        Assert.assertEquals(249, paginationResult.getLastPage().getEndIndex());

        List<Object> elements = Lists.newArrayList();
        PaginationResult<Object> lastPageResult = PaginationResult.of(elements, paginationResult.getCount(), paginationResult.getLastPage());
        Assert.assertFalse(lastPageResult.hasNextPage());
    }

    @Test
    public void testGetLastPageOnEmptyElements() {
        List<Object> elements = Lists.newArrayList();
        PaginationResult<Object> pageResult = PaginationResult.of(elements, 0, PaginationParameter.of(0, 20));
        PaginationParameter lastPage = pageResult.getLastPage();
        Assert.assertEquals(0, lastPage.getPageNumber());
    }

    @Test
    public void testPageSizeOne() {
        List<Object> elements = Lists.newArrayList();
        PaginationParameter page0 = PaginationParameter.of(0, 1);
        PaginationResult<Object> pageResult = PaginationResult.of(elements, 5, page0);

        Assert.assertEquals(5, pageResult.getCount());
        Assert.assertEquals(5, pageResult.getPageCount());
    }

    @Test
    public void testOfFullList() {
        String str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus " +
                "tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor";

        List<String> list = Arrays.asList(str.split(" "));
        Assert.assertEquals(22, list.size());

        {
            // All elements
            PaginationResult<String> page = PaginationResult.fromFullList(list, PaginationParameter.ALL);
            Assert.assertEquals(22, page.getElements().size());
            Assert.assertEquals(22, page.getCount());
            Assert.assertEquals(PaginationParameter.ALL, page.getCurrentPage());
        }

        {
            PaginationResult<String> page = PaginationResult.fromFullList(list, PaginationParameter.of(0, 2));
            Assert.assertEquals(2, page.getElements().size());
            Assert.assertEquals(Arrays.asList("Lorem", "ipsum"), page.getElements());
            Assert.assertEquals(22, page.getCount());
            Assert.assertEquals(11, page.getPageCount());
            Assert.assertEquals(PaginationParameter.of(0, 2), page.getCurrentPage());
        }

        {
            // 20 elements per page, first page : 20 elements
            PaginationResult<String> page = PaginationResult.fromFullList(list, PaginationParameter.of(0, 20));
            Assert.assertEquals(20, page.getElements().size());
            Assert.assertEquals(2, page.getPageCount());
        }

        {
            // 20 elements per page, second page : the 2 last elements
            PaginationResult<String> page = PaginationResult.fromFullList(list, PaginationParameter.of(1, 20));
            Assert.assertEquals(2, page.getElements().size());
            Assert.assertEquals(Arrays.asList("sed,", "dolor"), page.getElements());
            Assert.assertEquals(2, page.getPageCount());
        }

        {
            // pageSize strictly equals elements size : first page = all elements
            PaginationResult<String> page = PaginationResult.fromFullList(list, PaginationParameter.of(0, 22));
            Assert.assertEquals(22, page.getElements().size());
            Assert.assertEquals(1, page.getPageCount());
        }

        {
            // pageSize strictly equals elements size : second page = no element
            PaginationResult<String> page = PaginationResult.fromFullList(list, PaginationParameter.of(1, 22));
            Assert.assertEquals(0, page.getElements().size());
            Assert.assertEquals(1, page.getPageCount());
        }

        {
            // larger pageSize than elements
            PaginationResult<String> page = PaginationResult.fromFullList(list, PaginationParameter.of(0, 50));
            Assert.assertEquals(22, page.getElements().size());
            Assert.assertEquals(1, page.getPageCount());
        }

        {
            // larger pageSize than elements : out of bound page
            PaginationResult<String> page = PaginationResult.fromFullList(list, PaginationParameter.of(1, 50));
            Assert.assertEquals(0, page.getElements().size());
            Assert.assertEquals(1, page.getPageCount());
        }

        {
            // larger pageSize than elements : insane page
            PaginationResult<String> page = PaginationResult.fromFullList(list, PaginationParameter.of(1980, 27));
            Assert.assertEquals(0, page.getElements().size());
            Assert.assertEquals(1, page.getPageCount());
        }

    }

}
