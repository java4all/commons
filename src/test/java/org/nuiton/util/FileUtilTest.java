/*
 * #%L
 * Nuiton Utils
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.nuiton.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created: 22 nov. 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 */
public class FileUtilTest { // FileUtilTest

    public static final long TIMESTAMP = System.nanoTime();

    @Rule
    public final TestName testName = new TestName();

    protected File parent;

    @Before
    public void setUp() {

        parent = FileUtil.getTestSpecificDirectory(
                getClass(),
                testName.getMethodName(), null, TIMESTAMP);

    }

    @Test
    public void testFind() throws Exception {
        List<File> result = FileUtil.find(new File("."), ".*FileUtil.*", true);
        Assert.assertTrue(result.size() != 0);
    }

    @Test
    public void testBasename() throws Exception {
        String result = FileUtil.basename(new File("/tmp/toto.xml"), ".xml");
        Assert.assertEquals("toto", result);
    }

    @Test
    public void testExtension() throws Exception {
        String result = FileUtil.extension(new File("/tmp/toto.xml"));
        Assert.assertEquals("xml", result);
        result = FileUtil.extension(new File("/tmp/toto.xml"), ".", "o");
        Assert.assertEquals("xml", result);
        result = FileUtil.extension(new File("/tmp/toto.xml"), "t", ".");
        Assert.assertEquals("o.xml", result);
    }

    public void testChangeExtension() throws IOException {
        String name = "toto.yo";
        String exepectedName = "toto.ya";
        String newExtension = "ya";
        String actualName = FileUtil.changeExtension(name, newExtension);

        Assert.assertEquals(exepectedName, actualName);

        File file = new File(parent, name);
        File expectedFile = new File(parent, exepectedName);
        File actualFile = FileUtil.changeExtension(file, newExtension);
        Assert.assertEquals(expectedFile, actualFile);
    }

    public void testGetRelativeFile() {
        File inputDirectory = new File(parent, "in");
        File outputDirectory = new File(parent, "out");
        File file = new File(inputDirectory, "yoyo.to");
        File expectedFile = new File(outputDirectory, file.getName());
        File actualFile = FileUtil.getRelativeFile(inputDirectory, outputDirectory, file);
        Assert.assertEquals(expectedFile, actualFile);

        file = new File(inputDirectory, "rep" + File.separator + "yoyo.to");
        expectedFile = new File(outputDirectory, "rep" + File.separator + file.getName());
        actualFile = FileUtil.getRelativeFile(inputDirectory, outputDirectory, file);
        Assert.assertEquals(expectedFile, actualFile);
    }

    @Test
    public void testGetFileFromPaths() {
        File actual = FileUtil.getFileFromPaths(parent, "target", "surefire-workdir", "FileUtil");
        File expected = new File(parent, "target" + File.separator + "surefire-workdir" + File.separator + "FileUtil");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetFileFromFQN() {
        File actual = FileUtil.getFileFromFQN(parent, "target.surefire-workdir.FileUtil");
        File expected = new File(parent, "target" + File.separator + "surefire-workdir" + File.separator + "FileUtil");
        Assert.assertEquals(expected, actual);
    }

} // FileUtilTest
